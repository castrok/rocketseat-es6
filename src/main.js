import api from './api';

class App {
    constructor() {
        this.repositories = [];
        this.formElement = document.getElementById('repo-form');
        this.listElement = document.getElementById('repo-list');
        this.inputElement = document.querySelector('input[name=repository]');
        this.registerHandlers();
    }

    //REGISTRA OS EVENTOS
    registerHandlers(){
        this.formElement.onsubmit = event => this.addRepository(event);
    }

    async addRepository(event) {
        event.preventDefault();
        const repoInput = this.inputElement.value;

        if (repoInput.length === 0)
            return;


        const response = await api.get(`/repos/${repoInput}`);

        console.log(response);

        this.repositories.push({
            name: 'rockerseat.com.br',
            description: 'Tire a sua ideia do papel e dê vida a sua startup',
            avatar_url: 'https://avatars0.githubusercontent.com/u/28929274?v=4',
            html_url:  'https://github.com/rocketseat/rocketseat.com.br',
        });
        this.render();
    }

    render() {
        this.listElement.innerHTML = '';
        this.repositories.forEach(repo => {
            let imgElement = document.createElement('img');
            imgElement.setAttribute('src', repo.avatar_url);

            let titleElement = document.createElement('strong');
            titleElement.appendChild(document.createTextNode(repo.name));

            let descriptionElement = document.createElement('p');
            descriptionElement.appendChild(document.createTextNode(repo.description));

            let linkElement = document.createElement('a');
            imgElement.setAttribute('target', '_blank');
            linkElement.appendChild(document.createTextNode("Acessar"));


            let listElement = document.createElement('li');
            listElement.appendChild(imgElement);
            listElement.appendChild(titleElement);
            listElement.appendChild(descriptionElement);
            listElement.appendChild(linkElement);

            this.listElement.appendChild(listElement);
        });
    }
}

new App();
